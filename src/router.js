import { createBrowserRouter } from "react-router-dom";
import Form from "./components/Form";
//import Template from './components/Template';
import Table from "./components/Table";
import Tableapi from "./components/Tableapi";
import Data from "./components/Data";

//import LoginForm from "./auth/Login";
//import Admin from "./auth/Admin";
//import User from "./auth/User";

const router = createBrowserRouter([
    { path: '/form', element: <Form/> },
    //{ path: '/temp', element: <Template/> },
    { path: '/table', element: <Table/> },
    { path: '/table-api', element: <Tableapi/> },
    { path: '/', element: <Data/> },
    
    //{ path: '/', element: <LoginForm/> },
    //{ path: '/admin', element: <Admin/> },
    //{ path: '/user', element: <User/> },

    
]);

export default router; 