
import { configureStore } from "@reduxjs/toolkit";
import tableReducer from './tableSlice';
import authReducer from './authSlice'

const store = configureStore({
    reducer: {
        table: tableReducer,
        auth: authReducer
    }
});

export default store;
