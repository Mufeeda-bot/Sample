
import { createSlice } from "@reduxjs/toolkit";

export const authSlice = createSlice({
  name: 'auth',
  initialState: {
    role: null, // Initially set to null
    user: null, // Initially set to null
    admin: null // Initially set to null
  },
  reducers: {
    
    setUser: (state, action) => {
      state.user = action.payload;
      state.role='user';
    },
    setAdmin: (state, action) => {
      state.admin = action.payload;
      state.role ='admin';
    },
  }
});

export const {  setUser, setAdmin, clearRole, clearUser, clearAdmin } = authSlice.actions;


export default authSlice.reducer;
