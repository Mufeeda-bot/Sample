
import { createSlice } from "@reduxjs/toolkit";

export const tableSlice = createSlice({
    name: 'table',
    initialState: {
        data: [],
        loading: false,
        error: null,
    },
    reducers: {
        fetchDataStart: (state) => {
            state.loading = true;
            state.error = null;
            console.log("Fetch data start");
        },
        fetchDataSuccess: (state, action) => {
            state.loading = false;
            state.data = action.payload;
            console.log("Fetch data success:", action.payload);
        },
        fetchDataFailure: (state, action) => {
            state.loading = false;
            state.error = action.payload;
            console.error("Fetch data failure:", action.payload);
        },
        
    }
});

export const { fetchDataStart, fetchDataSuccess, fetchDataFailure} = tableSlice.actions;

export default tableSlice.reducer;
