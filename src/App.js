import { BrowserRouter, Routes, Route } from "react-router-dom";
import Form from "./components/Form";
//import Template from './components/Template';
import Table from "./components/Table";
import Tableapi from "./components/Tableapi";
import Data from "./components/Data";

//import LoginForm from "./auth/Login";
import Admin from "./auth/Admin";
import User from "./auth/User";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/form" element={<Form />} />
        {/* <Route path="/temp" element={<Template />} /> */}
        <Route path="/table" element={<Table />} />
        <Route path="/table-api" element={<Tableapi />} />
        <Route path="/data" element={<Data />} />
        {/* <Route path="/" element={<LoginForm />} /> */}
         <Route path="/admin" element={<Admin />} /> 
         <Route path="/user" element={<User />} /> 
      </Routes>
    </BrowserRouter>
  );
}

export default App;
