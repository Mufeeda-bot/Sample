import React from 'react';
import LoginForm from './Login';
import { useDispatch } from 'react-redux'; 

function User() {
  const dispatch = useDispatch();

  return (
    <div>
      <h2>User Login Page</h2>
      <LoginForm dispatch={dispatch} role="user" />
    </div>
  );
}

export default User;
