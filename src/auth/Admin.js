import React from 'react';
import LoginForm from './Login';
import { useDispatch } from 'react-redux'; 
//import { clearRoleAfterTime } from '../store/authSlice';

function Admin() {
  const dispatch = useDispatch();
  
  return (
    <div>
      <h2>Admin Login Page</h2>
      <LoginForm dispatch={dispatch} role="admin" />
    </div>
  );
}

export default Admin;
