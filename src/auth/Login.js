import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { setAdmin, setUser } from '../store/authSlice';

const users = [
  { username: 'user', password: '1234', role: 'user' }
];

const admins = [
  { username: 'admin', password: '4321', role: 'admin' }
];

function LoginForm({ dispatch, role }) {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');
  const navigate = useNavigate();

  const handleSubmit = (e) => {
    e.preventDefault();
    if (role === 'admin') {
      const admin = admins.find(admin => admin.username === username && admin.password === password);
      if (admin) {
        dispatch(setAdmin(admin));
        setError('');
        navigate('/data');
      } else {
        setError('Invalid username or password');
      }
    } else if (role === 'user') {
      const user = users.find(user => user.username === username && user.password === password);
      if (user) {
        dispatch(setUser(user));
        setError('');
        navigate('/data');
      } else {
        setError('Invalid username or password');
      }
    }
  };

  return (
    <div className="container" style={{ width: '400px' }}>
      <form onSubmit={handleSubmit}>
        <div className="mb-3">
          <label className="form-label">Username:</label>
          <input type="text" className="form-control" value={username} onChange={(e) => setUsername(e.target.value)} />
        </div>
        <div className="mb-3">
          <label className="form-label">Password:</label>
          <input type="password" className="form-control" value={password} onChange={(e) => setPassword(e.target.value)} />
        </div>
        {error && <div className="text-danger mb-3">{error}</div>}
        <button type="submit" className="btn btn-primary">Login</button>
      </form>
    </div>
  );
}

export default LoginForm;
