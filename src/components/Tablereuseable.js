
import React from 'react';
import { MaterialReactTable, useMaterialReactTable } from 'material-react-table';

function Tablereuseable({ data, columns }) {
    const table = useMaterialReactTable({
        columns,
        data,
    });

    return (
        <div style={{ height: 400, width: '100%' }}>
            <MaterialReactTable table={table} />
        </div>
    );
}

export default Tablereuseable;