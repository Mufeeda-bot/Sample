import React, { useState } from 'react';
import { Dialog, DialogTitle, DialogContent, DialogActions, Button, TextField } from '@mui/material';

const Edit = ({ rowData, onClose, open }) => {
  const [editedData, setEditedData] = useState(rowData);

  const handleInputChange = (event) => {
    const { name, value } = event.target;
  
    // Check if the name contains dot notation indicating nested state
    if (name.includes('.')) {
      const [parentKey, childKey] = name.split('.');
      setEditedData(prevData => ({
        ...prevData,
        [parentKey]: { ...prevData[parentKey], [childKey]: value }
      }));
    } else {
      // If not nested state, update directly
      setEditedData(prevData => ({
        ...prevData,
        [name]: value
      }));
    }
  };
  

  const handleUpdate = () => {
    onClose(); 
  };

  const handleClose = () => {
    onClose(); // Close the edit dialog without saving
  };

  return (
    <Dialog open={open} onClose={handleClose}>
      <DialogTitle>Edit Details</DialogTitle>
      <DialogContent>
        <TextField
          label="First Name"
          name="name.firstName"
          value={editedData.name.firstName}
          onChange={handleInputChange}
          fullWidth
          margin="normal"
        />
        <TextField
          label="Last Name"
          name="name.lastName"
          value={editedData.name.lastName}
          onChange={handleInputChange}
          fullWidth
          margin="normal"
        />
        <TextField
          label="Address"
          name="address"
          value={editedData.address}
          onChange={handleInputChange}
          fullWidth
          margin="normal"
        />
        <TextField
          label="City"
          name="city"
          value={editedData.city}
          onChange={handleInputChange}
          fullWidth
          margin="normal"
        />
        <TextField
          label="State"
          name="state"
          value={editedData.state}
          onChange={handleInputChange}
          fullWidth
          margin="normal"
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={handleUpdate} variant="contained" color="primary">Update</Button>
        <Button onClick={handleClose} variant="contained" color="secondary">Cancel</Button>
      </DialogActions>
    </Dialog>
  );
};

export default Edit;
