import React from 'react';
import { useForm } from "react-hook-form"
import { useState } from "react";
import { useNavigate } from "react-router-dom";

function Form(){
    const { register, handleSubmit, formState: { errors } } = useForm();
    const [showSuccess, setShowSuccess] = useState(false);
    const navigate = useNavigate();

    const onSubmit = (data) => {
        console.log(data);
        const isFormValid = Object.values(data).every(value => value !== '');
        if (isFormValid) {
            setShowSuccess(true);
           navigate("/table");
        }
    }
    return(
        <div className="d-flex justify-content-center align-items-center" style={{ height: '100vh'}}>
             <div className="card" style={{ width: '500px' }}>
             <div className="card-body">
             {showSuccess && (
                        <div className="alert alert-success mt-3" role="alert">
                            Success! Form submitted successfully.
                            
                        </div>
                    )}
                <form onSubmit={handleSubmit(onSubmit)}>
                    <h1>Form</h1>
                    <div className="form-group">
                    <label>Name</label>
                    <input type="text" className="form-control" placeholder="name" {...register("name",{required:"Name is required"})}></input>
                    {errors.name && <p className="text-danger">{errors.name.message}</p>}
                    </div>
                    <div className="form-group">
                    <label>Password</label>
                    <input type="password" className="form-control" placeholder="password" {...register("password",{required:"password is required", minLength:{value:4, message: 'password must be more than 4 characters '}, maxLength: {value:10,message:'Password cannot exceed more than 10 characters'}})}></input>
                    {errors.password && <p className="text-danger">{errors.password.message}</p>}
                    </div>
                    <div className="form-group">
                    <label>Date of Birth</label>
                    <input type="date" className="form-control" placeholder="Dob" {...register("dob",{required:"Date of birth is required"})}></input>
                    {errors.dob && <p className="text-danger">{errors.dob.message}</p>}
                    </div>
                    <div>
                    <label>Gender:</label>
                    <div className="form-check form-check-inline">
                                <label className="form-check-label">
                                    <input type="radio" className="form-check-input" value="male" {...register("gender", { required: "Gender is required" })} /> Male
                                </label>
                    </div>
                    <div className="form-check form-check-inline">
                                <label className="form-check-label">
                                    <input type="radio" className="form-check-input" value="female" {...register("gender", { required: "Gender is required"})} /> Female
                                </label>
                            </div>
                            {errors.gender && <p className="text-danger">{errors.gender.message}</p>}
                    </div>
                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>
                
             </div>
             </div>

        </div>
    )

}
export default Form;