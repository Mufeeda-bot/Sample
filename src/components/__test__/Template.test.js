import React from 'react';
import { render, fireEvent, waitFor, screen } from '@testing-library/react';
import Template from '../Temp';


describe('Template component', () => {
  it('submits form data', async () => {
    render(<Template />);
    
    // Select a template
    fireEvent.change(screen.getByRole('combobox'), { target: { value: '62' } });
    
    // Wait for template data to be fetched and displayed
    await waitFor(() => {
      expect(screen.getByText('Mock template content')).toBeInTheDocument();
    });
    
    // Add some content to the editor
    fireEvent.change(screen.getByRole('textbox'), { target: { value: 'Some content' } });
    
    // Submit the form
    fireEvent.click(screen.getByRole('button', { name: 'Submit' }));
    
    // Wait for the form submission to complete
    await waitFor(() => {
      // Add assertions to verify the form submission logic
      // For example, you can expect that an element indicating successful submission is present
      expect(screen.getByText('Form submitted successfully')).toBeInTheDocument();
      // Or you can mock an API call and expect it to be called with the correct data
      
    });
  });
});
