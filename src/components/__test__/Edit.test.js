import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import Edit from '../Edit';

describe('Edit component', () => {
  const mockRowData = {
    name: {
      firstName: 'John',
      lastName: 'Doe',
    },
    address: '123 Main St',
    city: 'Anytown',
    state: 'CA',
  };

  it('updates state when input changes', () => {
    render(<Edit rowData={mockRowData} onClose={() => {}} open={true} />);
    
    const firstNameInput = screen.getByLabelText('First Name');
    fireEvent.change(firstNameInput, { target: { value: 'Jane' } });
    expect(firstNameInput).toHaveValue('Jane');
  });

  it('calls onClose when cancel button is clicked', () => {
    const handleClose = jest.fn();
    render(<Edit rowData={mockRowData} onClose={handleClose} open={true} />);
    
    fireEvent.click(screen.getByRole('button', { name: 'Cancel' }));
    expect(handleClose).toHaveBeenCalledTimes(1);
  });

  it('calls onClose when update button is clicked', () => {
    const handleUpdate = jest.fn();
    render(<Edit rowData={mockRowData} onClose={handleUpdate} open={true} />);
    
    fireEvent.click(screen.getByRole('button', { name: 'Update' }));
    expect(handleUpdate).toHaveBeenCalledTimes(1);
  });
});
