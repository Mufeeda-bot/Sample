import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import Viewapi from '../Viewapi';


describe('Viewapi component', () => {
    const onClose = jest.fn();
    const open = true;

it('calls onClose when Close button is clicked', () => {
    render(<Viewapi  open={open} onClose={onClose} />);
    const closeButton = screen.getByText('Close');
    fireEvent.click(closeButton);
    expect(onClose).toHaveBeenCalled();
  });
});