import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import View from '../View';

describe('View component', () => {
 
  it('calls onClose when Close button is clicked', () => {
    const onClose = jest.fn();
    
    render(<View  open={true} onClose={onClose} />);
    
    fireEvent.click(screen.getByText('Close'));
    
    expect(onClose).toHaveBeenCalledTimes(1);
  });
});
