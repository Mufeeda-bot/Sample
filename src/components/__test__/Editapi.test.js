import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';

import Editapi from '../Editapi';

describe('Editapi component', () => {
  const onClose = jest.fn();
  const open = true;

  it('calls onClose when Update button is clicked', () => {
    render(
      <Editapi onClose={onClose} open={open} />
    );
    const updateButton = screen.getByRole('button', { name: 'Update' });
    fireEvent.click(updateButton);

    expect(onClose).toHaveBeenCalled();
  });
});
