import React from 'react';
import { render, screen, fireEvent, waitFor } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom"; 
import Form from "../Form";

describe("test the form component", () => {
    test('submit form with valid data', async () => {
        render(
            <BrowserRouter>
                <Form />
            </BrowserRouter>
        );

        // Fill out form fields
        fireEvent.change(screen.getByPlaceholderText('name'), { target: { value: 'John Doe' } });
        fireEvent.change(screen.getByPlaceholderText('password'), { target: { value: 'password' } });
        fireEvent.change(screen.getByPlaceholderText('Dob'), { target: { value: '2022-04-15' } });
        fireEvent.click(screen.getByLabelText('Male'));

        // Submit form
        fireEvent.click(screen.getByText('Submit'));

        // Wait for success message to appear
        await waitFor(() => {
            expect(screen.getByText(/Success! Form submitted successfully/i)).toBeInTheDocument();
        });
    });
});
