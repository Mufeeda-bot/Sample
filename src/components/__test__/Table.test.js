import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import Table from '../Table';

describe('Table component', () => {
  it('opens the edit dialog when the Edit icon is clicked', () => {
    render(<Table />);

    // Click the Edit icon
    fireEvent.click(screen.getAllByTestId('EditIcon')[0]); 
    
    // Check if the Edit dialog is opened
    const editIcons = screen.getAllByTestId('EditIcon');
fireEvent.click(editIcons[0]); // Click the first Edit icon found

  });
});
