import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';

import Data from '../Data';



describe('Data component', () => {
  

  it('opens the view dialog when the View icon is clicked', async () => {
    render(<Data />);

    const viewIcon = screen.getByLabelText('View');
    fireEvent.click(viewIcon);
    // Check if the view dialog is opened
    expect(screen.getByText('Product Details')).toBeInTheDocument();
 
  });

  it('opens the edit dialog when the Edit icon is clicked', async () => {
    render(<Data />);

    const editIcon = screen.getByLabelText('Edit');
    fireEvent.click(editIcon);
    // Check if the edit dialog is opened
    expect(screen.getByText('Edit Details')).toBeInTheDocument();
    
  });

});
