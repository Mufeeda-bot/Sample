import React from 'react';
import { render, fireEvent, waitFor, screen } from '@testing-library/react';
import Tableapi from '../Tableapi';
import axios from 'axios';

jest.mock('axios');

describe('Tableapi component', () => {
  beforeEach(() => {
    axios.get.mockResolvedValue({
      data: {
        data: [
          {
            name: 'Product 1',
            price: { current_price: 100, currency: 'USD' },
            description: 'Description 1',
            categories: [{ name: 'Category 1' }],
            supplier: { name: 'Supplier 1' },
            images: [{ image: 'image_url_1' }],
          },
          {
            name: 'Product 2',
            price: { current_price: 200, currency: 'EUR' },
            description: 'Description 2',
            categories: [{ name: 'Category 2' }],
            supplier: { name: 'Supplier 2' },
            images: [{ image: 'image_url_2' }],
          },
        ],
      },
    });
  });

  it('renders product list and handles actions', async () => {
    render(<Tableapi />);

    // Wait for the data to be loaded and displayed
    await waitFor(() => {
      expect(screen.getByText('Product List')).toBeInTheDocument();
     
    });

    // Click on the View icon
    fireEvent.click(screen.getAllByTestId('VisibilityIcon')[0]);
    expect(screen.getByText('View Product: Product 1')).toBeInTheDocument();

    // Click on the Edit icon
    fireEvent.click(screen.getAllByTestId('EditIcon')[0]);
    expect(screen.getByText('Edit Product: Product 1')).toBeInTheDocument();
  });
});
