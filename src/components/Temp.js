import React, { useState, useEffect } from "react";
import axios from "axios";
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";
import { useForm } from "react-hook-form";

function Template() {
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
    setValue,
    watch
  } = useForm();

  const selectedTemplateId = watch("template");

  const [toolbarOptions, setToolbarOptions] = useState([
    ["bold", "italic", "underline", "strike", "blockquote"],
    [{ header: 1 }],
    [{ list: "ordered" }, { list: "bullet" }],
    [{ indent: "-1" }, { indent: "+1" }],
    [{ size: ["small", false, "large", "huge"] }],
    [{ color: [] }, { background: [] }],
    [{ font: [] }],
    [{ align: [] }],
    ["link", "image"]
  ]);

  const [editorContent, setEditorContent] = useState("");

  useEffect(() => {
    const fetchData = async () => {
      try {
        if (selectedTemplateId) {
          const response = await axios.get(
            `https://api.campaign.smartsolutionsme.com/template/${selectedTemplateId}`
          );
          console.log("Response data:", response.data);

          if (response.data.data.content) {
            console.log("Selected template content:", response.data.data.content);
            setEditorContent(response.data.data.content);
            setValue("editorContent", response.data.data.content); // Set form value
          } else {
            console.log("Content property not found in the response data.");
          }

          if (response.data.toolbarOptions) {
            setToolbarOptions(response.data.toolbarOptions);
          }
        }
      } catch (error) {
        console.error("Error fetching template:", error);
      }
    };

    fetchData();
  }, [selectedTemplateId, setValue]);

  const onSubmit = (data) => {
    console.log(data); // You can handle form submission here
  };

  return (
    <div>
      <form onSubmit={handleSubmit(onSubmit)}>
        <select
          {...register("template", { required: true })}
        >
          <option value="">Select Template</option>
          <option value="62">Template 1</option>
          <option value="63">Template 2</option>
          <option value="58">Template 3</option>
          <option value="64">Template 4</option>
          {/* Add other options based on available templates */}
        </select>
        {errors.template && <span>This field is required</span>}

        <ReactQuill
          key={editorContent} // Adding key prop to force re-render
          value={editorContent}
          onChange={setEditorContent}
          modules={{ toolbar: toolbarOptions }}
          
        />
        
        <button type="submit">Submit</button>
      </form>
    </div>
  );
}

export default Template;
