import React from 'react';
import { Dialog, DialogTitle, DialogContent, DialogActions, Button } from '@mui/material';

const View = ({ rowData, open, onClose }) => {
  
  const handleClose = () => {
    onClose();
  };
  const { name, address, city, state } = rowData?.original || {};

  const { firstName, lastName } = name || {};
  return (
    <Dialog open={open} onClose={handleClose}> 
    <DialogTitle>Details</DialogTitle>
    <DialogContent>
      <p><b>First Name: </b>{firstName}</p>
      <p><b>Last Name: </b>{lastName}</p>
      <p><b>Address: </b>{address}</p>
      <p><b>City:</b> {city}</p>
      <p><b>State: </b>{state}</p>
    </DialogContent>
    <DialogActions>
      <Button onClick={handleClose}>Close</Button>
    </DialogActions>
  </Dialog>
  );
};

export default View;
