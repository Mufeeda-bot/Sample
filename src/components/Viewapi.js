import React from 'react';
import { Dialog, DialogTitle, DialogContent, DialogActions, Button } from '@mui/material';

const Viewapi = ({ rowData, open, onClose }) => {
  const handleClose = () => {
    onClose();
  };

  const { name, description, price, categories, images, supplier } = rowData?.original || {};

  return (
    <Dialog open={open} onClose={handleClose} fullWidth maxWidth="sm"> 
      <DialogTitle>Product Details</DialogTitle>
      <DialogContent>
        <div style={{ marginBottom: '20px' }}>
          <p><b>Name:</b> {name}</p>
          <p><b>Price:</b> {price ? `${price.current_price} ${price.currency}` : 'N/A'}</p>
          <p><b>Description:</b> {description}</p>
          <p><b>Category:</b> {categories && categories.length > 0 ? categories[0].name : 'N/A'}</p>
          <p><b>Supplier:</b> {supplier ? supplier.name : 'N/A'}</p>
        </div>
        <div>
          <img src={images && images.length > 0 ? images[0].image : ''} alt="Product" style={{ width: '100px', height: '100px', borderRadius: '4px', border: '1px solid #ddd' }} />
        </div>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose} color="primary">Close</Button>
      </DialogActions>
    </Dialog>
  );
};

export default Viewapi;
