import React, { useState, useEffect } from 'react';
import { Dialog, DialogTitle, DialogContent, DialogActions, Button, TextField } from '@mui/material';

const Editapi = ({ rowData, onClose, open }) => {
  const [editedData, setEditedData] = useState({
    name: '',
    price: {
      current_price: '',
      currency: '',
    },
    description: '',
    categories: '',
    supplier: {
      name: '',
    },
    images: '',
  });

  useEffect(() => {
    if (rowData) {
      setEditedData({
        name: rowData.name || '',
        price: {
          current_price: rowData.price?.current_price || '',
          currency: rowData.price?.currency || '',
        },
        description: rowData.description || '',
        categories: rowData.categories && rowData.categories.length > 0 ? rowData.categories[0].name : '',
        supplier: {
          name: rowData.supplier?.name || '',
        },
        images: rowData.images && rowData.images.length > 0 ? rowData.images[0].image : '',
      });
    }
  }, [rowData]);

  const handleInputChange = (event) => {
    const { name, value, files } = event.target;

    if (name.includes('.')) {
      const [parentKey, childKey] = name.split('.');
      setEditedData((prevData) => ({
        ...prevData,
        [parentKey]: { ...prevData[parentKey], [childKey]: value },
      }));
    } else {
      setEditedData((prevData) => ({
        ...prevData,
        [name]: value,
      }));
    }

    if (files && files[0]) {
      const reader = new FileReader();
      reader.onload = function (e) {
        setEditedData((prevData) => ({
          ...prevData,
          images: e.target.result,
        }));
      };
      reader.readAsDataURL(files[0]);
    }
  };

  const handleUpdate = () => {
    onClose();
  };

  const handleClose = () => {
    onClose();
  };

  return (
    <Dialog open={open} onClose={handleClose}>
      <DialogTitle>Edit Details</DialogTitle>
      <DialogContent>
        <TextField
          label="Name"
          name="name"
          value={editedData.name}
          onChange={handleInputChange}
          fullWidth
          margin="normal"
        />
        <TextField
          label="Current Price"
          name="price.current_price"
          value={editedData.price.current_price}
          onChange={handleInputChange}
          fullWidth
          margin="normal"
        />
        <TextField
          label="Currency"
          name="price.currency"
          value={editedData.price.currency}
          onChange={handleInputChange}
          fullWidth
          margin="normal"
        />
        <TextField
          label="Description"
          name="description"
          value={editedData.description}
          onChange={handleInputChange}
          fullWidth
          margin="normal"
        />
        <TextField
          label="Category"
          name="categories"
          value={editedData.categories}
          onChange={handleInputChange}
          fullWidth
          margin="normal"
        />
        <TextField
          label="Supplier"
          name="supplier.name"
          value={editedData.supplier.name}
          onChange={handleInputChange}
          fullWidth
          margin="normal"
        />
        {/* Image Preview with File Upload */}
        <div style={{ marginTop: '20px' }}>
          <input
            accept="image/*"
            id="contained-button-file"
            multiple
            type="file"
            style={{ display: 'none' }}
            onChange={handleInputChange}
          />
          {editedData.images && (
            <img src={editedData.images} alt="Preview" style={{ marginTop: '10px', maxWidth: '100%' }} />
          )}
          <label htmlFor="contained-button-file">
            <Button variant="contained" component="span">
              Upload Image
            </Button>
          </label>
        </div>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleUpdate} variant="contained" color="primary">
          Update
        </Button>
        <Button onClick={handleClose} variant="contained" color="secondary">
          Cancel
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default Editapi;
