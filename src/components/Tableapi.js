import React, { useMemo,useState, useEffect } from 'react';
import { MaterialReactTable,useMaterialReactTable,} from 'material-react-table';
import axios from 'axios';
import EditIcon from '@mui/icons-material/Edit';
import VisibilityIcon from '@mui/icons-material/Visibility';
import Viewapi from './Viewapi';
import Editapi from './Editapi'
  
function Tableapi() {
    const [data, setData] = useState([]);
    const [selectedRow, setSelectedRow] = useState(null);
  const [isViewOpen, setIsViewOpen] = useState(false);
  const [isEditOpen, setIsEditOpen] = useState(false); // State to manage the edit dialog
  const [editedData, setEditedData] = useState(null); 

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await axios.get("https://api.smartpos.busotc.com/products");
                setData(response.data.data);
                console.log(response.data.data);
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }

        fetchData();
    }, []);

    const columns = useMemo(
        () => [
          {
            accessorKey: 'name',
            header: 'Name',
            size: 150,
          },
          {
            accessorKey: 'price.current_price',
            header: 'Current Price',
            size: 150,
          },
          {
            accessorKey: 'price.currency', 
            header: 'currency',
            size: 200,
          },
          
          {
            accessorKey: 'description',
            header: 'description',
            size: 150,
          },
          {
            accessorKey: 'categories',
            header: 'Category',
            size: 150,
            Cell: ({ row }) => {
                const category = row.original.categories && row.original.categories.length > 0
                    ? row.original.categories[0].name || 'N/A'
                    : 'N/A';
                return <span>{category}</span>;
            }
        },
       
          {
            accessorKey: 'supplier.name',
            header: 'supplier',
            size: 150,
          },
          
          {
            accessorKey: 'images[0].image',
            header: 'Image',
            size: 150,
            Cell: ({ row }) => (
              <img src={row.original.images[0].image} alt="Product" style={{ width: '50px', height: '50px' }} />
            ),
          },
          {
            accessorKey: 'actions',
            header: 'Actions',
            size: 100,
            Cell: ({ row }) => (
              <div>
                 <VisibilityIcon onClick={() => handleView(row)} style={{ cursor: 'pointer', marginRight:'10px' }} />
                <EditIcon onClick={() => handleEdit(row)} style={{ cursor: 'pointer' }} />
           
              </div>
            ),
          },
        ],
         
        [],
      );
    const table = useMaterialReactTable({
        columns,
        data, 
      });
      const handleEdit = (row) => {
        setSelectedRow(row);
        setEditedData(row.original); // Set the original data to be edited
        setIsEditOpen(true); // Open the edit dialog
      };
    
      const handleView = (row) => {
        setSelectedRow(row);
        setIsViewOpen(true); 
      };
      const closePopup = () => {
        setSelectedRow(null);
        setIsViewOpen(false);
        setIsEditOpen(false); 
      };
      
    return (
        <div style={{ height: 400, width: '100%' }}>
            <h1>Product List</h1>
            <MaterialReactTable table={table} />;
            {selectedRow && <Viewapi rowData={selectedRow} onClose={closePopup} open={isViewOpen}/>}
            {selectedRow && editedData && <Editapi rowData={editedData} onClose={closePopup} open={isEditOpen} 
        
            />
        }

        </div>
    );
}

export default Tableapi;
