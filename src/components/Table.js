import React, { useState } from 'react';
import { useMemo } from 'react';
import { MaterialReactTable, useMaterialReactTable } from 'material-react-table';
import EditIcon from '@mui/icons-material/Edit';
import VisibilityIcon from '@mui/icons-material/Visibility';
import View from './View';
import Edit from './Edit'; 

const data = [
    {
      name: {
        firstName: 'John',
        lastName: 'Doe',
      },
      address: '261 Erdman Ford',
      city: 'East Daphne',
      state: 'Kentucky',
    },
    {
      name: {
        firstName: 'Jane',
        lastName: 'William',
      },
      address: '769 Dominic Grove',
      city: 'Columbus',
      state: 'Ohio',
    },
    {
      name: {
        firstName: 'Joe',
        lastName: 'Doe',
      },
      address: '566 Brakus Inlet',
      city: 'South Linda',
      state: 'West Virginia',
    },
];

function Table(){
  const [selectedRow, setSelectedRow] = useState(null);
  const [isViewOpen, setIsViewOpen] = useState(false);
  const [isEditOpen, setIsEditOpen] = useState(false); // State to manage the edit dialog
  const [editedData, setEditedData] = useState(null); // State to store edited data

    const columns = useMemo(
        () => [
          {
            accessorKey: 'name.firstName', 
            header: 'First Name',
            size: 150,
          },
          {
            accessorKey: 'name.lastName',
            header: 'Last Name',
            size: 150,
          },
          {
            accessorKey: 'address', 
            header: 'Address',
            size: 200,
          },
          {
            accessorKey: 'city',
            header: 'City',
            size: 150,
          },
          {
            accessorKey: 'state',
            header: 'State',
            size: 150,
          },
          {
            accessorKey: 'actions',
            header: 'Actions',
            size: 100,
            Cell: ({ row }) => (
              <div>
                 <VisibilityIcon onClick={() => handleView(row)} style={{ cursor: 'pointer', marginRight:'10px' }} />
                <EditIcon onClick={() => handleEdit(row)} style={{ cursor: 'pointer' }} />
           
              </div>
            ),
          },
        ],
        [],
      );
      const table = useMaterialReactTable({
        columns,
        data, 
      });
      const handleEdit = (row) => {
        setSelectedRow(row);
        setEditedData(row.original); // Set the original data to be edited
        setIsEditOpen(true); // Open the edit dialog
      };
    
      const handleView = (row) => {
        setSelectedRow(row);
        setIsViewOpen(true); 
      };
      const closePopup = () => {
        setSelectedRow(null);
        setIsViewOpen(false);
        setIsEditOpen(false); 
      };

    return(
        <div>
          <MaterialReactTable table={table} />
          {selectedRow && <View rowData={selectedRow} onClose={closePopup} open={isViewOpen}/>}
          {selectedRow && <Edit rowData={editedData} onClose={closePopup} open={isEditOpen}/>} {/* Render the edit dialog */}
        </div>
    )

}
export default Table;
