import React, { useMemo, useEffect, useState } from 'react';
import axios from 'axios';
import Tablereuseable from './Tablereuseable';
import EditIcon from '@mui/icons-material/Edit';
import VisibilityIcon from '@mui/icons-material/Visibility';
import Viewapi from './Viewapi';
import Editapi from './Editapi';
import { connect } from 'react-redux';
import { fetchDataSuccess,fetchDataStart,fetchDataFailure } from '../store/tableSlice';

function Data({ data, fetchDataStart, fetchDataSuccess, fetchDataFailure,role }) {
  const [selectedRow, setSelectedRow] = useState(null);
  const [isViewOpen, setIsViewOpen] = useState(false);
  const [isEditOpen, setIsEditOpen] = useState(false);
  const [editedData, setEditedData] = useState(null);

    useEffect(() => {
      fetchData();
  }, []);

  const fetchData = async () => {
      fetchDataStart();
      try {
          const response = await axios.get("https://api.smartpos.busotc.com/products");
          fetchDataSuccess(response.data.data);
          console.log("Response data:", response.data.data);
      } catch (error) {
          fetchDataFailure(error.message);
      }
  };

    const columns = useMemo(
        () => [
            {
                accessorKey: 'name',
                header: 'Name',
                size: 150,
            },
            {
                accessorKey: 'price.current_price',
                header: 'Current Price',
                size: 150,
            },
            {
                accessorKey: 'price.currency', 
                header: 'currency',
                size: 150,
              },
              
              {
                accessorKey: 'description',
                header: 'description',
                size: 150,
              },
              {
                accessorKey: 'categories',
                header: 'Category',
                size: 150,
                Cell: ({ row }) => {
                    const category = row.original.categories && row.original.categories.length > 0
                        ? row.original.categories[0].name || 'N/A'
                        : 'N/A';
                    return <span>{category}</span>;
                }
            },
           
              {
                accessorKey: 'supplier.name',
                header: 'supplier',
                size: 150,
              },
              
              {
                accessorKey: 'images[0].image',
                header: 'Image',
                size: 150,
                Cell: ({ row }) => {
                    const imageUrl = row.original.images?.[0]?.image;
                    if (!imageUrl) {
                        return <img src="placeholder_image_url" alt="Placeholder" />;
                    }
                    return (
                        <img 
                            src={imageUrl}
                            alt="Product" 
                            style={{ width: '50px', height: '50px' }} 
                        />
                    );
                },
            },
            
            
            {
              accessorKey: 'actions',
              header: 'Actions',
              size: 150,
              Cell: ({ row }) => (
                <div>
                  <VisibilityIcon onClick={() => handleView(row)} style={{ cursor: 'pointer', marginRight: '10px' }} />
                  {/* Render the Edit button only if the role is 'admin' */}
                  {role === 'admin' && <EditIcon onClick={() => handleEdit(row)} style={{ cursor: 'pointer' }} />}
                </div>
              ),
            },
          ],
          [role] 
        );
    const handleEdit = (row) => {
        setSelectedRow(row);
        setEditedData(row.original); 
        setIsEditOpen(true); 
      };
    
      const handleView = (row) => {
        setSelectedRow(row);
        setIsViewOpen(true); 
      };
      const closePopup = () => {
        setSelectedRow(null);
        setIsViewOpen(false);
        setIsEditOpen(false); 
      };

    return (
        <div>
            <h1>Product Details</h1>
            <Tablereuseable data={data} columns={columns} />
            {selectedRow && <Viewapi rowData={selectedRow} onClose={closePopup} open={isViewOpen}/>}
            {selectedRow && editedData && <Editapi rowData={editedData} onClose={closePopup} open={isEditOpen} />}
        </div>
    );
}
const mapStateToProps = (state) => ({
  data: state.table.data,
  role: state.auth.role 
});

const mapDispatchToProps = {
  fetchDataStart,
  fetchDataSuccess,
  fetchDataFailure,
};
export default connect(mapStateToProps, mapDispatchToProps)(Data);